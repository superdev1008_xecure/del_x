## React Native GoogleMap, MapBox
Front-end of the Delivery app with react native.

## Preview

<img width="250" height="450" style="float: left" src="https://drive.google.com/uc?export=view&id=1wuXVIT3sYEPcbQKOoC4bZdLZGWHkiuqj" />
<img width="250" height="450" style="float: left" src="https://drive.google.com/uc?export=view&id=1XFLtQk17aZPkSVF_d-aO1j9-gjsG0nAp" />
<img width="250" height="450" style="float: left" src="https://drive.google.com/uc?export=view&id=1qaQpIU-eINOJb-ZIGLIQkCtnuyWCMi_v" />

## Run
```sh
git clone https://gitlab.com/superdev1008_xecure/del_x.git
cd del_x
npm install
react-native run-ios | react-native run-android
```